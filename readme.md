# java-dev-compose

## Description
The purpose of this project is to setup docker containers to support the development of Java projects. 

## Containers
Below are a list of containers which the docker-compose file aims to start up.
### postgres
This container is the DB which holds any data you attach any service to.
### pgAdmin4
This container is a web application which allows you to browse any postgres DB.
### SonarQube
This container contains SonarQube, which will analyse any code deployments through GitLab. You can also attach
the service to Intellij or any other application.

## Setup
To setup these containers, you will need to make sure virtualization is enabled on your 
machine.

First install docker desktop from [here](https://www.docker.com/products/docker-desktop "Download").

Once docker desktop is up and running, you will be required to enable the usage of the WSL 2 based engine in the
settings of the application.

![Enable WSL 2 based engine in Docker Desktop settings](/resources/enable-wsl2.png "Enable WSL 2 based engine in Docker Desktop settings")

To ensure the settings are correct before spinning up the containers, run the following command to change the max map
count required for SonarQube.
```shell
wsl -d docker-desktop
sysctl -w vm.max_map_count=262144
```


Next run the following command to spin up the containers:
```shell
docker-compose up -d
```

On first run, the SonarQube container may not spin up properly first, and this next section will aim to fix the issue.

## DB setup
To start adding databases, first a server connection needs to be created. To open pgAdmin4, open up https://localhost:5050.

Next login with the password '**postgres**'.

Next right click on servers and create a server.

On the General tab, give it the name '**postgres**', and on the 'Connection' tab, enter the host as '**postgres**'.
This will add the postgres container as a server.

![Create server connection](/resources/add-server.png "Create server connection")

Finally create a DB connection by opening up the server just added, right clicking on 'Databases' and creating a new database called '**sonar**'. This is done because the SonarQube container depends on this database.

![Create sonar database](/resources/create-sonar-db.png "Create sonar database")

Finally, on Docker desktop, click the start button on the SonarQube container and it will use the sonar DB as it's database.

## Exploring SonarQube
To open SonarQube when it is running, open up http://localhost:9000.

The default username and password is '**admin**'' and '**admin**'.

If you wish to generate a token for any connection, head to http://localhost:9000/account/security. From here you can 
generate a token which can be used to authorize an application.

C:\Users\perso\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup